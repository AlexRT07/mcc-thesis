% Chapter 1

\chapter{Introducción} % Main chapter title

\label{Chap:Intro} % For referencing the chapter elsewhere, use \ref{Intr} 

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separted from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}

%----------------------------------------------------------------------------------------

\section{Preliminares}
En este documento de tesis se presentará el desarrollo de un sistema de estimación de pose de un vehículo aéreo no tripulado o \textbf{VANT}, a partir de dos fuentes de información: un sensor inercial o \textbf{IMU}, y el sistema de navegación satelital \textbf{\textbf{GPS}, por sus siglas en inglés} en modo \textit{Real-Time Kinematics} (\textbf{RTK}, por sus siglas en inglés). Dado un estado del VANT, conformado por información de localización y orientación en un momento determinado, la pose será actualizada con las mediciones obtenidas de las fuentes de información mencionadas, pasanddo a través de un filtro de Kalman en su variante Unscented, que permite modelar sistemas no lineales. Este trabajo es una continuación del proyecto de tesis de licenciatura de~\citet{turriza2017}.

\section{Importancia del tema}
La correcta estimación de la orientación y posición de un vehículo respecto de un marco de referencia fijo sobre el cual se encuentra navegando, permite tener conocimiento de un determinado estado en que se encuentra, y que es útil al momento de determinar las correcciones que se deben aplicar ante cualquier fuente de ruido que pueda alterar la posición del VANT, por ejemplo, los movimientos causados por el viento.

\section{Planteamiento del Problema}
La medición de la posición y orientación está basada en sensores electrónicos entre los que se encuentran los inerciales y GPS, que no están exentos a presentar distintos tipos de ruido. Errores en la medición de la orientación del vehículo autónomo derivan en la aplicación de correcciones no requeridas, que pueden desestabilizar el vehículo, y que requieren de un consumo de energía mayor.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Figures/VANT}
\caption[VANT]{Vehículo Aéreo No Tripulado en misión de vuelo.}
\label{fig:Prec}
\end{figure}

\section{Trabajos previos}
En el trabajo de~\citet{turriza2017}, se mejora el rango de precisión de GPS hasta los $7 cm$ en el modo RTK para asistir en el control de vuelo de un VANT. Sin embargo, dicha información es insuficiente para el cálculo de la pose de un vehículo no tripulado, dado que dicho sensor proporciona la localización de una antena en un marco de referencia terrestre, pero es imposible determinar la orientación del cuerpo. Además, la frecuencia de actualización de un dispositivo GPS es bastante lenta ($~5 Hz$), quedando lapsos de tiempo considerables sin nueva información de localización.\\

En el artículo de \citet{Zihajehzadeh2015}, los autores hacen uso de un \textit{filtro de Kalman en cascada} para combinar la información de GPS con la de un IMU. Entre las ventajas expuestas, indican ser entre un 70\% y 80\% más eficientes computacionalmente, alcanzando una precisión de entre $2$ y $3$ grados para la orientación, y $1.27m$ y $1.53m$ para la posición. Mencionan, a su vez, que RTK GPS puede ser implementado para obtener una mejora sustancial.\\

\citet{Eling2015}, en su artículo, plantea el uso de un filtro de Kalman extendido con un estado de dieciséis elementos para la combinación de la información proporcionada por una IMU y un GPS, obteniendo una precisión mejor que $5 cm$ y $\ang{0.5}$ en posicionamiento y orientación, respectivamente.\\

El artículo presentado por \citet{Deilamsalehy2018} muestra la implementación de un filtro de Kalman extendido, adaptativo difuso (\textit{fuzzy adaptive extended Kalman filter}), donde combinan información proporcionada por un IMU, una cámara y un sensor LIDAR (\textit{Laser Imaging Detection and Ranging}, detección y medición de imágenes por láser) para una estimación de pose y posición de un VANT. Sus resultados presentan un error promedio de $0.0025m$ en posición y $0.001 rad$.\\

Puede notarse que los trabajos previos suelen ocupar el filtro de Kalman extendido (\textit{Extended Kalman Filter}, \textbf{EKF}, por sus siglas en inglés) como opción para realizar la predicción de la posición del VANT, pero de acuerdo a \citet{St-Pierre2004}, cuando se fusionan datos entre un GPS y una IMU, el filtro de Kalman Unscented (\textit{Unscented Kalman Filter}, \textbf{UKF}, por sus siglas en inglés), presenta un mejor desempeño que el primero. Además, RTK GPS es una fuente de geolocalización mucho más precisa que una única antena GPS, según \citet{turriza2017}, por lo que se tiene una ventaja \textit{a priori} en la presición respecto de la solución GPS utilizada por los artículos previos.

\subsubsection{Propuesta}
Se propone utilizar un sensor inercial para determinar la orientación en el espacio del VANT, así como aprovechar la información que proporciona dicho instrumento para realizar una combinación con la información de RTK GPS y hacer más robusta la información de posicionamiento, aprovechando la alta tasa de información recibida ($~100 Hz$), utilizando un filtro de Kalman Unscented.

%la modelación de un estado que contenga la información de pose de un vehículo aéreo no tripulado, junto con las ecuaciones de transición en el tiempo mediante un filtro de Kalman, para la predicción con alta precisión del mismo, haciendo uso de sensores inerciales y de navegación cinética satelital.

\section{Hipótesis}
Mediante el uso de un filtro de Kalman en su variante Unscented, se puede estimar con alta precisión la información de la posición y pose de un vehículo aéreo no tripulado en un determinado momento.

\section{Objetivo}
\subsubsection{General}
El objetivo general de este trabajo es el de realizar la estimación robusta de la posición y la pose de un vehículo aéreo no tripulado mediante el uso de un filtro bayesiano y mediciones de sensores inerciales y de navegación cinética satelital.

\subsubsection{Objetivos específicos}
\begin{itemize}
\item Implementación del sistema de navegación cinética satelital (RTK GPS) de la tesis de \citet{turriza2017}.
\item Ajustar el sistema RTK GPS para minimizar peso y dimensiones.
\item Proponer un vector de estado $\textbf{x}$, que contendrá la información de la pose del VANT.
\item Obtener las ecuaciones de transición en el tiempo de un estado anterior $\textbf{x}_{t-1}$ a un estado siguiente $\textbf{x}_t$.
\item Construir un vector de medición $\textbf{z}$, que contenga la lectura de los sensores.
\item Calcular una función $H(\textbf{x})$ que realice un mapeo del vector de estado $\textbf{x}$ hacia el vector de mediciones $\textbf{z}$.
\item Implementar una unidad de medición inercial para estimar la orientación y medir la aceleración del sistema.
\item Integrar módulos.
\item Evaluar los resultados obtenidos.
\end{itemize}

\section{Descripción del documento}

Este documento de tesis se divide en las siguientes secciones:

\begin{itemize}
\item \textbf{Introducción}: Da una descripción en general del proyecto de tesis, muestra el estado del arte, describe la importancia del tema, plantea la hipótesis y presenta los objetivos.

\item \textbf{Marco Teórico}: En este capítulo se sientan las bases sobre las cuales se fundamentan los métodos a utilizar, así como se da una descripción general del hardware y software a utilizar.

\item \textbf{Metodología}: En esta sección se presenta el desarrollo de la estrategia a utilizar, con base en lo planteado en el Marco Teórico. También, se describen las condiciones de funcionamiento, y las ecuaciones de los filtros bayesianos utilizados.

\item \textbf{Resultados}: Se realiza una descripción de los resultados obtenidos en la rutina de experimentación.

\item \textbf{Conclusiones}: En este capítulo se muestra un resumen acerca de los resultados obtenidos, sus interpretaciones y se habla acerca del rendimiento general del sistema propuesto.
\end{itemize}