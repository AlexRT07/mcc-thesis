% Chapter 2

\chapter{Diseño del Sistema}
\label{Chap:DisHard} % For referencing the chapter elsewhere, use \ref{Chapter2} 

%----------------------------------------------------------------------------------------

\section{Introducción}

En este capítulo se presenta información referente al funcionamiento general del sistema desarrollado. Se detallarán los circuitos electrónicos utilizados, así como se expondrá la lógica del software diseñado para control del IMU y RTK GPS a través del filtro de Kalman. Al final, se presentarán los parámetros utilizados en el filtro de Kalman Unscented.\\

\section{RTK GPS}

\subsubsection{Hardware}

Para usar el kit de evaluación de GPS de Ublox C94-M8P en modo RTK, se requiere del uso de dos sensores en continua comunicación. El designado como receptor base requiere ser configurado en el software proporcionado por el fabricante para ingresar a su memoria las coordenadas en que será colocado. Posterior a eso, solo basta con proporcionarle alimentación ya sea vía el puerto micro USB con que cuenta el circuito de evaluación, o bien, a través de una fuente de tensión en el pin designado para proporcionar energía por vía externa. El GPS seleccionado como \textit{Rover} requerirá, además de alimentación como su contraparte fija, conexión vía el puerto micro USB al puerto USB de la BeagleBone Black, y comunicar la información de corrección del puerto serial DB9 al pin $P9_26$ de la computadora BeagleBone, mismo que funciona como pin RXD del puerto serial 1. El circuito que permite esta última comunicación se muestra en la Figura~\ref{fig:BB2M94}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{BB-M94_2}
	\caption{Diagrama de conexión del circuito de conversión de señales RS232 del GPS designado como Rover a la BeagleBone Black.}
	\label{fig:BB2M94}
\end{figure}

\subsubsection{Software}

Para usar el pin $P9_26$ en modo de comunicación serial UART, se deben ingresar los siguientes comandos a la consola de la BeagleBone~\citep{Molloy2019}:

\begin{lstlisting}
config-pin P9_26 uart
\end{lstlisting}

Un paquete de programas es requerido para procesar toda la información de los GPS, llamado RTKLIB\footnotemark. Dentro de dicho conjunto de programas, se debe de compilar y configurar RTKRCV para su ejecución. Dentro del archivo de configuración, debe especificarse un puerto sobre el cual otro programa a través de sockets pueda conectarse, el puerto sobre el que recibirá la información del \textit{Rover}, el tipo de mensajes que recibirá (que son de tipo UBX), el tipo de mensajes de corrección (que son de tipo RTCM3), el puerto donde recibirá la información de corrección (que será /dev/ttyO1, por el pin seleccionado en el diagrama).\\

\footnotetext{Se recomienda el uso de la versión 2.4.3 a partir de la beta 20, dado que añaden soporte al tipo de mensajes que utiliza la tarjeta de evaluación de GPS.}

Se creó un conjunto de bibliotecas en C++ que pueden manejar la información recibida de RTKRCV a través del puerto indicado en el archivo de configuración del mismo, y que es útil para guardar la información de posicionamiento y hora en un archivo externo. La biblioteca tiene también otras utilidades para manejo de pantallas LCD, push buttons, leds, y puertos de entrada/salida en general, que no serán requeridos en el desarrollo de esta tesis. Las bibliotecas pueden ser consultadas y descargadas de \href{https://github.com/Alex-Turriza/RTKLIB-LLH-handler-with-BeagleBone-Black-Support}{www.github.com/Alex-Turriza/RTKLIB-LLH-handler-with-BeagleBone-Black-Support}.

\section{IMU Bosch BNO055}

\subsubsection{Hardware}

El sensor inercial Bosch BNO055 utiliza el protocolo $I^{2}C$ para comunicarse con un microcontrolador externo, por lo que se designaron como pines de control en la BeagleBone Black a $P9\_17$ y $P9\_18$, designados como pines de comunicación para este protocolo, de acuerdo al manual de~\citet{coley2013beaglebone}, quedando el circuito como indica la Figura~\ref{fig:BB2BNO}.

\begin{figure}[H]	\includegraphics[width=\linewidth]{BB-BNO055}
	\caption{Diagrama de conexión del sensor inercial a la BeagleBone Black.}
	\label{fig:BB2BNO}
\end{figure}

\subsubsection{Software}

Los pines $P9\_17$ y $P9\_18$ deben ser configurados para su uso en modo $I^2C$, utilizando los siguientes comandos de la BeagleBone:

\begin{lstlisting}
config-pin P9_17 i2c
config-pin P9_18 i2c
\end{lstlisting}

Un controlador fue diseñado en el lenguaje de programación C++ para la configuración y toma de datos del dispositivo, mismo que puede ser consultado en la liga:~\href{https://github.com/arturoemx/BNO055-BBB_IMU-Driver}{www.github.com/arturoemx/BNO055-BBB\_IMU-Driver}.\\

Siguiendo el manual de~\citet{boschbno055}, se configuraron los registros necesarios para el uso en modo \textit{NDOF} (\textit{nine degrees of freedom}, nueve grados de libertad, por sus siglas en inglés), que entrega los cuaterniones en función de su posición de origen para la orientación, el vector de velocidades angulares, así como la aceleración lineal (con la aceleración de la gravedad eliminada), ambos en los tres ejes $x$, $y$  y $z$, mediciones que serán ingresadas al filtro de Kalman Unscented.

\subsubsection{Preprocesamiento de datos}
Tras tomar y guardar las mediciones de aceleración del IMU, y previo a su introducción al filtro de Kalman Unscented, se toma una muestra estadística de los mismos, aplicando un cálculo recursivo para una media y varianza por cada 10 mediciones. La fórmula que se utiliza para calcular dicho promedio~\citep{Kelly2013} es:

\begin{equation}~\label{Eq:AccProm}
\overline{\underline{x}}_{k + 1} = \frac{k \overline{\underline{x}}_k + \underline{x}_{k+1}}{k+1}
\end{equation}

Si el resultado no supera un determinado umbral, se asume una aceleración cero en dicho instante de tiempo.

\subsubsection{Estimación del error de orientación entre cuaterniones unitarios}
\label{SubSub:QuatErrMeas}

Sean $\overset{\sim}{q}_1$ y $\overset{\sim}{q}_2$ cuaterniones unitarios. La métrica utilizada que permite medir una distancia entre dos cuaterniones en función del ángulo $\Delta \theta$ existente entre ambos se indica en la ecuación~\ref{Eq:DistEq}:

\begin{equation}~\label{Eq:DistEq}
d = 1 - \left( \overset{\sim}{q}_1 \cdot \overset{\sim}{q}_2 \right)^2
\end{equation}

donde $\overset{\sim}{q}_1 \cdot \overset{\sim}{q}_2$ es el producto punto entre los dos cuaterniones, y el resultado $d$ se encuentra dentro del rango entre $0$ y $1$: el primero indica que no existe diferencia entre los ángulos representados por los dos cuaterniones; y el último, indica que los vectores apuntan en direcciones totalmente opuestas. La Figura~\ref{fig:distQuat} muestra cómo oscila la distancia medida entre cuaterniones en función del ángulo más corto entre cuaterniones, de acuerdo a la ecuación~\ref{Eq:DistEq}.

\begin{figure}[H]	
	\centering
	\includegraphics[width=0.8\linewidth]{distanceQuat}
	\caption{Valor de la distancia entre dos cuaterniones unitarios en función del ángulo más corto que les separa (en radianes).}
	\label{fig:distQuat}
\end{figure}

La ecuación~\ref{Eq:DistEq} es equivalente a:

\begin{equation}
d = \frac{1 - Cos(\Delta \theta)}{2}
\end{equation}

mismo que puede desarrollarse para obtener el ángulo $\Delta \theta$ que separa a los cuaterniones:

\begin{equation}
\Delta \theta = Cos^{-1}\left(2 \ (\overset{\sim}{q}_1 \cdot \overset{\sim}{q}_2)^2 - 1 \right)
\end{equation} 

\section{Filtro de Kalman Unscented}

El código del filtro de Kalman fue escrito en GNU Octave, y consta de los siguientes parámetros:\\

El vector de estados $\textbf{x}$ se conforma de los siguientes elementos:
	
\begin{equation}
	\textbf{x} = [x \ \ y \ \ z \ \ \dot{x} \ \ \dot{y} \ \ \dot{z} \ \ a_x \ \ a_y \ \ a_z \ \ \omega_x \ \ \omega_y \ \ \omega_z \ \ q_0 \ \ q_1 \ \ q_2 \ \ q_3]
\end{equation}

siendo un vector de 16 elementos, descritos a continuación:

\begin{itemize}
	\item $x$, $y$, $z$ son las coordenadas en el espacio de la posición del vehículo.
	
	\item $\dot{x}$, $\dot{y}$, $\dot{z}$ son las primeras derivadas de la posición, correspondiendo a la velocidad del vehículo en cada eje.
	
	\item $a_x$, $a_y$, $a_z$ son las aceleraciones en los respectivos ejes del vehículo.
	
	\item $\omega_x$, $\omega_y$, $\omega_z$ son las velocidades angulares en los ejes.

	\item $q_0$, $q_1$, $q_2$, $q_3$ son los cuaterniones que indican la orientación actual del dispositivo, siendo $q_0$ la parte real.
	
\end{itemize}

El vector de medición o de salida $\textbf{z}$ contiene los siguientes elementos:

\begin{equation}
\textbf{z} = [a_x \ \ a_y \ \ a_z \ \ \omega_x \ \ \omega_y \ \ \omega_z \ \ q_0 \ \ q_1 \ \ q_2 \ \ q_3]
\end{equation}

descritos a continuación:

\begin{itemize}
	
	\item $a_x$, $a_y$, $a_z$ son las aceleraciones en los respectivos ejes del vehículo.
	
	\item $\omega_x$, $\omega_y$, $\omega_z$ son las velocidades angulares en los ejes.
	
	\item $q_0$, $q_1$, $q_2$, $q_3$ son los cuaterniones que indican la orientación actual del dispositivo, siendo $q_0$ la parte real.
	
\end{itemize}

La función $f()$ de transformación o actualización se define de la siguiente manera:

\begin{eqnarray}
f(\textbf{x}) = 
\begin{bmatrix}
 p_{t+1} &=& p_{t} + \dot{p}_{t} \Delta t + \frac{1}{2} a_{t}  \Delta t^2
 \\ \dot{p}_{t+1} &=& \dot{p}_{t} + a_{t} \Delta t
 \\ a_{t+1} &=& a_t
 \\ \omega_{t+1} &=& \omega_{t}
 \\ q_{t+1} &=& q_{t} 
\end{bmatrix}
\end{eqnarray}

donde $p$ y $\dot{p}$ indican la posición y velocidad del cuerpo. $\Delta t$ es la diferencia de tiempo entre mediciones del IMU, cuyo valor es de $\Delta t = 100^{-1}$

La función $H()$ que mapea un estado a la salida fue definido de la siguiente manera:

\begin{eqnarray}
H(\textbf{x}) = 
\begin{bmatrix}
\textbf{x}(7) &=& a_x \\
\textbf{x}(8) &=& a_y \\
\textbf{x}(9) &=& a_z\\
\textbf{x}(10) &=& \omega_x\\
\textbf{x}(11) &=& \omega_y\\
\textbf{x}(12) &=& \omega_z\\
\textbf{x}(13) &=& q_0\\
\textbf{x}(14) &=& q_1\\
\textbf{x}(15) &=& q_2\\
\textbf{x}(16) &=& q_3
\end{bmatrix}
\end{eqnarray}

La matriz de covarianza de proceso es inicializada como la matriz identidad de dimensiones $16 \times 16$. Las matrices de ruido aditivo para el proceso y de ruido de medición se inicializan como matrices diagonales con valor de $0.015$.

\section{Conclusión}

En este capítulo se detallaron aspectos de la construcción de hardware del equipo requerido en el proyecto de tesis, así como de los controles de software de los dispositivos, junto con los algoritmos y los valores de inicialización de las variables.\\

En el capítulo~\ref{Chap:DisExp} se presenta la lógica del experimento a utilizar, la forma de corroborar los datos y se da una descripción de los resultados de forma gráfica como numérica.\\